#include "addform.h"
#include "ui_addform.h"

#include <QLabel>
#include <QLineEdit>
#include <QSpinBox>
#include <QDoubleSpinBox>
#include <QDateTimeEdit>
#include <QCheckBox>
#include <QComboBox>

#include "../../Interfaces/Utility/i_user_task_data_ext.h"
#include "../../Interfaces/Utility/i_user_task_date_data_ext.h"
#include "../../Interfaces/Utility/i_user_task_repeat_data_ext.h"
#include "../../Interfaces/Utility/i_user_task_pomodoro_data_ext.h"

constexpr int dayMSecs = 1000 * 60 * 60 * 24;

AddForm::AddForm(QWidget *parent) :
	QWidget(parent),
	ui(new Ui::AddForm)
{
	ui->setupUi(this);
	connect(ui->buttonAdd, SIGNAL(clicked(bool)), SLOT(addTask()));
	connect(ui->buttonAccept, SIGNAL(clicked(bool)), SLOT(AcceptChanges()));
	connect(ui->buttonDelete, SIGNAL(clicked(bool)), SLOT(deleteTask()));

	//	connect(addForm, SIGNAL(OnClose()), this, SLOT(OnAddFormClosed()));
	//	connect(addForm, &AddForm::OnDelete, this, &UserTaskManagerView::deleteItem);

	ui->checkBoxDetails->setEnabled(false);
	ui->frameEdit->setVisible(false);
	ui->buttonAccept->setEnabled(false);
	ui->buttonDelete->setEnabled(false);
	ui->dateEditLater->setDateTime(QDateTime::currentDateTime());

	connect(ui->newTaskLineEdit, &QLineEdit::textChanged, this, [=](const QString& text) {
		ui->buttonAdd->setEnabled(!text.isEmpty());
	});

	connect(ui->horizontalSliderStartAt, &QSlider::valueChanged, this, [=](int value) {
		auto percent = (float)value / ui->horizontalSliderStartAt->maximum();
		ui->timeEditStartAt->setTime(QTime::fromMSecsSinceStartOfDay(percent * dayMSecs));
	});

	gridLayout = new QGridLayout(ui->scrollAreaWidgetContents);
}

AddForm::~AddForm()
{
	ClearEditors();
	delete gridLayout;
	delete ui;
}

void AddForm::SetModel(DesignProxyModel *model)
{
	this->model = model;
}

void AddForm::showModelDataPrivate(const ExtendableItemDataMap& header,
        const ExtendableItemDataMap& itemData)
{
	int rowCount = 0;
	for(auto interfaceIter = header.begin(); interfaceIter != header.end(); ++interfaceIter)
	{
		for(auto fieldIter = interfaceIter.value().begin(); fieldIter != interfaceIter.value().end(); ++fieldIter)
		{
			QWidget *editWidget = GetStandardDataTypeEditor(
			                interfaceIter.key(), fieldIter.key(), itemData[interfaceIter.key()][fieldIter.key()], this);

			if(!editWidget)
				continue;

			auto label = new QLabel(fieldIter.key(), this);
			gridLayout->addWidget(label, rowCount, 0);
			gridLayout->addWidget(editWidget, rowCount, 1);

			m_editWidgets[interfaceIter.key()][fieldIter.key()] = {label, editWidget, fieldIter.value().typeId()};
			++rowCount;
		}
	}
}

void AddForm::ShowModelData(const QModelIndex &index)
{
	ClearEditors();
	currentModelIndex = index;

	bool isValid = currentModelIndex.isValid();
	QMap<Interface, QMap<QString, QVariant> > header = model->model->getHeader();
	QMap<Interface, QMap<QString, QVariant> > data = isValid ? model->model->getItem(index) : header;
	showModelDataPrivate(header, data);

	ui->buttonAccept->setEnabled(isValid);
	ui->buttonDelete->setEnabled(isValid);
	ui->newTaskLineEdit->setPlaceholderText(QString("%1 name...").arg(isValid ? "Subtask" : "Task"));

	ui->checkBoxDetails->setEnabled(isValid);
	if(!isValid && ui->checkBoxDetails->isChecked())
	{
		ui->checkBoxDetails->setChecked(false);
	}
}

QWidget* AddForm::GetStandardDataTypeEditor(
        const Interface& interface, const QString& fieldName, const QVariant& value, QWidget* parent)
{
	auto type = value.type();

	if(value.type() == QMetaType::Type::QByteArray)
		return nullptr;

	if(interface == INTERFACE(IUserTaskRepeatDataExtention) && fieldName == "repeatType")
	{
		auto widget = new QComboBox(parent);
		QStringList repeatTypeNames({"None", "Daily", "Weekly", "Monthly", "Yearly"});
		widget->addItems(repeatTypeNames);
		widget->setCurrentIndex(value.toInt());
		return widget;
	}

	if(interface == INTERFACE(IUserTaskPomodoroDataExtention) && fieldName == "pomodorosFinished")
	{
		auto widget = new QSpinBox(parent);
		widget->setValue(value.toInt());
		widget->setEnabled(false);
		widget->setButtonSymbols(QSpinBox::ButtonSymbols::NoButtons);
		return widget;
	}

	switch (type)
	{
	case QMetaType::Type::QString:
	{
		auto widget = new QLineEdit(parent);
		widget->setText(value.toString());
		return widget;
	}
	case QMetaType::Type::Int:
	case QMetaType::Type::UInt:
	case QMetaType::Type::LongLong:
	case QMetaType::Type::ULongLong:
	{
		auto widget = new QSpinBox(parent);
		widget->setValue(value.toInt());
		return widget;
	}
	case QMetaType::Type::Double:
	{
		auto widget = new QDoubleSpinBox(parent);
		widget->setValue(value.toInt());
		return widget;
	}
	case QMetaType::Type::QDateTime:
	{
		auto widget = new QDateTimeEdit(parent);
		widget->setCalendarPopup(true);
		widget->setDateTime(value.toDateTime());
		return widget;
	}
	case QMetaType::Type::Bool:
	{
		auto widget = new QCheckBox(parent);
		widget->setChecked(value.toBool());
		return widget;
	}
	default:
	{
		auto widget = new QLineEdit(parent);
		widget->setText(value.toString());
		return widget;
	}
	}
}

QVariant AddForm::GetDataFromStandardDataTypeEditor(
        const Interface& interface, const QString& fieldName, const QVariant& value, QWidget* editor)
{
	if(value.type() == QMetaType::Type::QByteArray)
		return value;

	if(interface == INTERFACE(IUserTaskRepeatDataExtention) && fieldName == "repeatType")
	{
		auto widget = qobject_cast<QComboBox*>(editor);
		return widget->currentIndex();
	}

	switch (value.type())
	{
	case QMetaType::Type::QString:
	{
		auto widget = qobject_cast<QLineEdit*>(editor);
		return widget->text();
	}
	case QMetaType::Type::Int:
	{
		auto widget = qobject_cast<QSpinBox*>(editor);
		return widget->value();
	}
	case QMetaType::Type::Double:
	{
		auto widget = qobject_cast<QDoubleSpinBox*>(editor);
		return widget->value();
	}
	case QMetaType::Type::QDateTime:
	{
		auto widget = qobject_cast<QDateTimeEdit*>(editor);
		return widget->dateTime();
	}
	case QMetaType::Type::Bool:
	{
		auto widget = qobject_cast<QCheckBox*>(editor);
		return widget->isChecked();
	}
	default:
	{
		auto widget = qobject_cast<QLineEdit*>(editor);
		return widget->text();
	}
	}
}

void AddForm::ClearEditors()
{
	for(auto interfaceIter = m_editWidgets.begin(); interfaceIter != m_editWidgets.end(); ++interfaceIter)
	{
		for(auto fieldIter = interfaceIter.value().begin(); fieldIter != interfaceIter.value().end(); ++fieldIter)
		{
			auto label = fieldIter.value().label;
			auto editWidget = fieldIter.value().editor;

			gridLayout->removeWidget(label);
			delete label;
			gridLayout->removeWidget(editWidget);
			delete editWidget;
		}
	}
	m_editWidgets.clear();
}

void AddForm::addTask()
{
	auto name = ui->newTaskLineEdit->text();
	QDateTime startDate;
	if(ui->radioButtonToday->isChecked())
	{
		startDate = QDateTime::currentDateTime();
	}
	else if(ui->radioButtonTomorrow)
	{
		startDate = QDateTime::currentDateTime().addDays(1);
	}
	else
	{
		startDate = ui->dateEditLater->dateTime();
	}
	startDate = startDate.addMSecs(ui->timeEditStartAt->time().msecsSinceStartOfDay());

	auto dueDate = startDate.addSecs(ui->spinBoxTakeTime->value() * 3600);

	int repeatType = RepeatType::NONE;
	if(ui->checkBoxRepeat->isChecked())
	{
		if(ui->radioButtonDaily->isChecked())
		{
			repeatType = RepeatType::DAILY;
		}
		else if(ui->radioButtonWeekly->isChecked())
		{
			repeatType = RepeatType::DAILY;
		}
		else if(ui->radioButtonMonthly->isChecked())
		{
			repeatType = RepeatType::DAILY;
		}
	}

	model->model->addItem({
		{INTERFACE(IUserTaskDataExtention), {{"name", ui->newTaskLineEdit->text()}}},
		{INTERFACE(IUserTaskDateDataExtention), {{"startDate", startDate}, {"dueDate", dueDate}}},
		{INTERFACE(IUserTaskRepeatDataExtention), {{"repeatType", repeatType}}},
	}, model->model->rowCount(currentModelIndex), currentModelIndex);

	ui->newTaskLineEdit->clear();
	emit onAddedTask(currentModelIndex);
}

void AddForm::deleteTask()
{
	if(currentModelIndex.isValid())
	{
		model->model->removeItem(currentModelIndex);
	}
	emit OnDelete();
}

void AddForm::AcceptChanges()
{
	ExtendableItemDataMap itemValues;
	for(auto interfaceIter = m_editWidgets.begin(); interfaceIter != m_editWidgets.end(); ++interfaceIter)
	{
		for(auto fieldIter = interfaceIter.value().begin(); fieldIter != interfaceIter.value().end(); ++fieldIter)
		{
			itemValues[interfaceIter.key()][fieldIter.key()] = GetDataFromStandardDataTypeEditor(
			                interfaceIter.key(), fieldIter.key(), fieldIter.value().type, fieldIter.value().editor);
		}
	}
	model->model->updateItem(currentModelIndex, itemValues);
	emit OnClose();
}

void AddForm::CancelChanges()
{
	hide();
	ClearEditors();
	emit OnClose();
}

bool AddForm::event(QEvent *event)
{
	switch (event->type()) {
	case QEvent::KeyRelease: {
		QKeyEvent *keyEvent = static_cast<QKeyEvent*>(event);
		switch (keyEvent->key()) {
		case Qt::Key_Enter:
			AcceptChanges();
			break;
		case Qt::Key_Escape:
			CancelChanges();
			break;
		default:
			return QWidget::event(event);
			break;
		}

	}
	break;
	default:
		return QWidget::event(event);
		break;
	}
}
