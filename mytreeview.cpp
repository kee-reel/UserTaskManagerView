#include "mytreeview.h"

#include "QHeaderView"

MyTreeView::MyTreeView(QWidget *parent) : QTreeView(parent)
{
	setAnimated(true);
	setWordWrap(true);
	setAutoScroll(true);
	setHeaderHidden(true);
	setDropIndicatorShown(true);
	setAlternatingRowColors(true);
	setDragDropOverwriteMode(false);

	setIndentation(8);
	setAutoExpandDelay(1000);

	setDragDropMode(DragDrop);
	setVerticalScrollMode(ScrollPerPixel);
	setSelectionBehavior(QAbstractItemView::SelectRows);
	setRootIsDecorated(true);

	setStyleSheet("QTreeView::indicator {width: 25px; height: 25px;}"
	        "QTreeView::indicator:unchecked {image: url(:/Res/radio_btn_off.png);}"
	        "QTreeView::indicator:checked {image: url(:/Res/radio_btn_on.png);}"
	);
	setExpandsOnDoubleClick(true);
	setFocusPolicy(Qt::StrongFocus);

	header()->setSectionResizeMode(QHeaderView::Fixed);
	header()->resizeSection(1, 50);
	header()->resizeSection(2, 50);

#ifdef Q_OS_ANDROID
	setSelectionMode(QAbstractItemView::SingleSelection);
	setDragEnabled(false);
	setAcceptDrops(false);
	auto port = viewport();
	port->installEventFilter(this);
	port->grabGesture(Qt::TapAndHoldGesture);
	QScroller::grabGesture(port, QScroller::LeftMouseButtonGesture);
#else
	setDragEnabled(true);
	setAcceptDrops(true);
#endif

	connect(this, &QTreeView::clicked, this, &MyTreeView::OnTreeViewItemCheck);
	connect(this, &QTreeView::pressed, this, &MyTreeView::OnTreeViewItemCheck);
}

// For Android drag and drop handling
bool MyTreeView::eventFilter(QObject *obj, QEvent *event)
{
	switch(event->type())
	{
	case QEvent::Gesture: {
		QGestureEvent *gestevent = static_cast<QGestureEvent *>(event);
		if (gestevent->gesture(Qt::TapAndHoldGesture)) {
			setDragEnabled(true);
			setAcceptDrops(true);
			setStyleSheet("QTreeView::item:selected{"
			        "    background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #7C8282, stop: 1 #C9D3D3);}"
			);
		}
	}
	break;

	case QEvent::Drop:
	case QEvent::MouseButtonRelease:
		setDragEnabled(false);
		setAcceptDrops(false);
		setStyleSheet("QTreeView::item:selected{"
		        "background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #D9E5E6, stop: 1 #C4D0D0);}"
		);
		break;
	default:
		break;
	}
	return QTreeView::eventFilter(obj, event);
}

void MyTreeView::mousePressEvent(QMouseEvent *event)
{
	QModelIndex index = indexAt(event->pos());
	if(index.isValid())
	{
		QTreeView::mousePressEvent(event);
	}
	else
	{
		selectionModel()->select(index, QItemSelectionModel::Deselect);
		emit clicked(index);
	}
}

void MyTreeView::OnTreeViewItemCheck(const QModelIndex &index)
{
	if(!index.isValid())
	{
		clearSelection();
		return;
	}
}
